# Memo Monitoring

### Installation

To install the plugin you can follow the steps described below.

1. Add access key to bitbucket repository.

This can be done in the repository by going to "Repository Settings" and the choose the menu option "Access keys".

2. Add the repository to `composer.json`

If the key `repositories` does not exists yet you will need to create it.
Take a look at the partial `composer.json` and add the missing parts.
```json
{
    "repositories": [
        {
            "type": "path",
            "url": "custom/plugins/*/packages/*",
            "options": {
                "symlink": true
            }
        },
        {
            "type": "path",
            "url": "custom/static-plugins/*",
            "options": {
                "symlink": true
            }
        },
        {
            "type": "vcs",
            "url": "git@bitbucket.org:memo_development/memomonitoringsw6.git"
        }
    ]
}
```

After you adapted the `composer.json` file you can require the plugin using composer with the following command.

```bash
composer require memo/monitoring-sw6
```

Now the Plugin should be available in the Shopware Backend.

### Configuration

In the plugin configuration you can set the following settings:

- E-Mail Address for Mail Monitoring - This section will regularly send a E-Mail to the given Mailaddress to check, if the Mail-Queue works.
- Saleschannel for Monitoring - This setting will decide from which sales channel to send the Emails from.
- Cronjobs which should be checked - Add the Cronjobs which you want to check here.

### Usage

#### Mail Monitoring

The E-Mail monitoring needs a system cronjob to work.
You can activate it by adding the following line to the crontab.
```bash
20 * * * * /.../php /.../bin/console memo:monitoring:check:emails
```

This send a E-Mail to shop-monitor@memo.support. Check if the E-Mail arrives there:

https://webmail.cyon.ch/
Login via 1Password

If that works, you can copy the subject and go to PRTG: http://192.168.0.33:8080/index.htm
Login in 1Password

Go to Geraete > Extern > E-Mail > Kunden Mail-Dienste > Shop E-Mail Checker and copy one of the existing sensors, like Puresense. (Right click the name and clone the sensor)

Name: Domain without tld -> like Puresense for puresense.ch

Go to the Settings and look for the field "Suchausdruck".
Here we can paste the Subject from the Test-EMail. The sensor will look for mails with that subject, if the "newest" one is older than 1 hour, there must be a problem with the e-mail system.

Afterwords, activate (unpause) the sensor -> Play icon at the top right.

#### Cronjob Monitoring

The Cronjobs can be checked by calling the following URL `https://example.ch/memo/healthreport/cronjobs`.

Add the new Sensor to PRTG:
```
Type: HTTP Daten (Erweitert)
Name: Cronjob-Kontrolle
URL: https://example.ch/memo/healthreport/cronjobs
"Abfrageintervall ueberschreiben" and set it to 15 minutes
```
