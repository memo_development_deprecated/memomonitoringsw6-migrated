<?php

namespace Memo\Monitoring\Controller;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskCollection;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskDefinition;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskEntity;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaymentMethodsCheckController
{
    /**
     * @var SystemConfigService
     */
    private $configService;

    /**
     * @var EntityRepository
     */
    private $paymentMethodRepository;

    /**
     * @var Context
     */
    private $context;

    public function __construct(SystemConfigService $configService, EntityRepository $paymentMethodRepository)
    {
        $this->configService = $configService;
        $this->paymentMethodRepository = $paymentMethodRepository;
        $this->context = Context::createDefaultContext();
    }

    /**
     * @Route("/memo/healthreport/payment_methods", name="memo_healthreport_payment_methods", defaults={"_routeScope"={"storefront"}})
     *
     * @return Response
     */
    public function check(): Response
    {
        $minCountActivePaymentMethods = $this->configService->get('MemoMonitoring.config.minCountActivePaymentMethods') ?? 0;

        $criteria = (new Criteria());
        $criteria->addAssociation('salesChannels');
        /** @var ScheduledTaskCollection $entities */
        $paymentMethods = $this->paymentMethodRepository->search($criteria, $this->context)->getEntities();
        $filteredPaymentMethods = [];

        foreach ($paymentMethods as $paymentMethod) {
            if ($paymentMethod->getActive() === false) {
                continue;
            }

            if ($paymentMethod->getSalesChannels()->count() === 0) {
                continue;
            }

            $filteredPaymentMethods[] = $paymentMethod;
        }

        $countActivePaymentMethods = count($filteredPaymentMethods);

        $result = [
            'prtg' => [
                'result' => [
                    [
                        'channel' => 'Count of active payment methods with sales channel',
                        'value' => count($filteredPaymentMethods)
                    ]
                ]
            ]
        ];

        if ($countActivePaymentMethods < $minCountActivePaymentMethods) {
            $result = [
                'prtg' => [
                    'error' => 1,
                    'text' => 'Count of active payment methods with sales channel lower than expected. Current: ' . $countActivePaymentMethods . ' | Configured minimum: ' . $minCountActivePaymentMethods,
                    'result' => [
                        [
                            'channel' => 'Count of active payment methods with sales channel',
                            'value' => count($filteredPaymentMethods)
                        ]
                    ]
                ]
            ];
        }

        return new JsonResponse($result);
    }
}
