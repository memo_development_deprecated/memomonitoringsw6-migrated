<?php

namespace Memo\Monitoring\Controller;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskCollection;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskDefinition;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskEntity;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CronjobCheckController
{
    /**
     * @var SystemConfigService
     */
    private $configService;

    /**
     * @var EntityRepository
     */
    private $scheduledTaskRepository;

    /**
     * @var Context
     */
    private $context;

    public function __construct(SystemConfigService $configService, EntityRepository $scheduledTaskRepository)
    {
        $this->configService = $configService;
        $this->scheduledTaskRepository = $scheduledTaskRepository;
        $this->context = Context::createDefaultContext();
    }

    /**
     * @Route("/memo/healthreport/cronjobs", name="memo_healthreport_cronjobs", defaults={"_routeScope"={"storefront"}})
     *
     * @return Response
     */
    public function check(): Response
    {
        $scheduledTaskIDs = $this->configService->get('MemoMonitoring.config.scheduledTasks');
        $criteria = (new Criteria());

        if (!is_null($scheduledTaskIDs)) {
            $criteria->addFilter(new EqualsAnyFilter('id', $scheduledTaskIDs));
        }

        $criteria->addAssociation('deadMessages');
        /** @var ScheduledTaskCollection $entities */
        $scheduledTasks = $this->scheduledTaskRepository->search($criteria, $this->context)->getEntities();
        $failedTasks = new ScheduledTaskCollection();
        $result = [
            'prtg' => [
                'result' => [
                    [
                        'channel' => 'Cronjobs successfully checked',
                        'value' => $scheduledTasks->count()
                    ]
                ]
            ],
        ];

        /** @var ScheduledTaskEntity $scheduledTask */
        foreach ($scheduledTasks as $scheduledTask) {
            $maxDelay = $scheduledTask->getRunInterval() + 900;
            $minDate = new \DateTime('-' . $maxDelay . ' seconds');

            if (
                (
                    $minDate > $scheduledTask->getLastExecutionTime()
                    && $scheduledTask->getStatus() !== ScheduledTaskDefinition::STATUS_INACTIVE
                )
                || $scheduledTask->getStatus() === ScheduledTaskDefinition::STATUS_FAILED
            ) {
                $failedTasks->add($scheduledTask);
            }
        }

        if ($failedTasks->count() > 0) {
            $failedTasksNames = [];

            foreach ($failedTasks as $failedTask) {
                $failedTasksNames[] = $failedTask->getName();
            }

            $result = [
                'prtg' => [
                    'error' => 1,
                    'text' => 'Problems detected for the following cronjob(s): ' . implode(', ', $failedTasksNames),
                ]
            ];
        }

        return new JsonResponse($result);
    }
}
