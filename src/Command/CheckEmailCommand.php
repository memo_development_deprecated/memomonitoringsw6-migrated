<?php
/**
 * Implemented by Media Motion AG team https://www.mediamotion.ch
 *
 * @copyright Media Motion AG https://www.mediamotion.ch
 * @license proprietary
 * @link https://www.mediamotion.ch
 */

namespace Memo\Monitoring\Command;

use Psr\Log\LoggerInterface;
use Shopware\Core\Content\Mail\Service\AbstractMailService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SalesChannel\SalesChannelCollection;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class CheckEmailCommand extends Command
{
    /**
     * @var SystemConfigService
     */
    private $configService;

    /**
     * @var AbstractMailService
     */
    private $mailService;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var EntityRepository
     */
    private $salesChannelRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CheckEmailCommand constructor.
     *
     * @param SystemConfigService $configService
     * @param AbstractMailService $mailService
     * @param EntityRepository $salesChannelRepository
     * @param LoggerInterface $logger
     */
    public function __construct(SystemConfigService $configService, AbstractMailService $mailService, EntityRepository $salesChannelRepository, LoggerInterface $logger)
    {
        $this->configService = $configService;
        $this->mailService = $mailService;
        $this->context = Context::createDefaultContext();
        $this->salesChannelRepository = $salesChannelRepository;
        $this->logger = $logger;

        parent::__construct();
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('memo:monitoring:check:emails')
            ->setDescription('Checks E-Mail state')
            ->setHelp('Checks E-Mails can be sent');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $toMail = $this->configService->get('MemoMonitoring.config.memomonitoremailaddress');
        $salesChannelId = $this->configService->get('MemoMonitoring.config.salesChannelId');
        $errorMessage = [];

        if (is_null($toMail) || $toMail === '') {
            $errorMessage[] = 'Receiver Mail has not been configured!';
        }

        if (!filter_var($toMail, FILTER_VALIDATE_EMAIL)) {
            $errorMessage[] = 'Configured Mail is not a valid mail address!';
        }

        if (is_null($salesChannelId)) {
            $errorMessage[] = 'Sales Channel has not been configured!';
        }

        if (count($errorMessage)) {
            $this->logErrors($errorMessage);
            return 1;
        }

        $criteria = (new Criteria())->addAssociations(['translations', 'domains']);
        $criteria->addFilter(new EqualsFilter('id', $salesChannelId));
        /** @var SalesChannelCollection $entities */
        $entities = $this->salesChannelRepository->search($criteria, $this->context)->getEntities();

        if ($entities->count() === 0) {
            $errorMessage[] = 'Sales Channel has not been found!';
            $this->logErrors($errorMessage);
            return 1;
        }

        $salesChannel = $entities->first();
        $shopName = $salesChannel->getName();
        $domains = $salesChannel->getDomains();

        if ($domains->count() === 0) {
            $errorMessage[] = 'Sales Channel has no Domains configured!';
            $this->logErrors($errorMessage);
            return 1;
        }

        $shopUrl = $domains->first()->getUrl();

        $data = new ParameterBag();
        $data->set(
            'recipients',
            [
                $toMail => 'PRTG Sensor'
            ]
        );

        $data->set('senderName', $shopName);
        $data->set('contentHtml', '<html><p>Das ist ein Testmail vom Shop: ' . $shopName . '<br>Erreichbar unter: ' . $shopUrl . '</p></html>');
        $data->set('contentPlain', 'Das ist ein Testmail vom Shop: ' . $shopName . ' Erreichbar unter: ' . $shopUrl);
        $data->set('subject', 'Testmail - ' . $shopName);
        $data->set('salesChannelId', $salesChannelId);

        try {
            $this->mailService->send(
                $data->all(),
                $this->context
            );
        } catch (\Exception $e) {
            $this->logger->error(
                "Could not send mail:\n"
                . $e->getMessage() . "\n"
                . 'Error Code:' . $e->getCode() . "\n"
                . "Template data: \n"
                . json_encode($data->all()) . "\n"
            );
            return 1;
        }

        return 0;
    }

    /**
     * Logs the given Errors.
     *
     * @param array $errors
     */
    private function logErrors(array $errors)
    {
        foreach ($errors as $error) {
            $this->logger->error('MemoMonitoring: ' . $error);
        }
    }
}
