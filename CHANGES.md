# Changes

##### 3.0.0

- FEATURE: Added Shopware 6.5 compatibility.

##### 2.0.3

- BUGFIX: Fixed Shopware 6.4 compatibility.

##### 2.0.2

- FEATURE: Fixed Composer Versioning.

##### 2.0.1

- FEATURE: Enhanced PRTG Logic for cronjob checker.

##### 2.0.0

- FEATURE: Added Shopware 6.4 compatibility.

##### 1.0.1

- FEATURE: Fixed Composer versioning.

##### 1.0.1

- FEATURE: Fixed required Shopware version.
- FEATURE: Added Documentation.

##### 1.0.0

- FEATURE: Added Cronjob checking and added Email Checking.
- FEATURE: Basic Plugin Structure.
